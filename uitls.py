import bcrypt
from typing import List, Tuple, Union, Dict, Optional


def hash_password(plaintext: bytes) -> str:
    return bcrypt.hashpw(plaintext, bcrypt.gensalt()).decode()


def check_password(password: Union[str, bytes], hashed: Union[str, bytes]) -> bool:
    if isinstance(password, str):
        password = password.encode('utf-8')
    if isinstance(hashed, str):
        hashed = hashed.encode('utf-8')

    return bcrypt.checkpw(password, hashed)
