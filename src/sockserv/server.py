import abc
import logging
import selectors
import socket
import threading
from typing import List, Type, Tuple, Union


class AbstractRequestHandler:

    def __init__(self, client_socket, client_address, server):
        self.request: socket.socket = client_socket
        self.address: Tuple[str, int] = client_address
        self.server = server

        self.logger = logging.getLogger('request_handler')

        self._init()

    def handle(self) -> None:
        pass

    def _init(self) -> None:
        pass

    def _teardown(self) -> None:
        pass


class FileStreamRequestHandler(AbstractRequestHandler):
    """
     Introduces file IO wrappers around client socket
    """

    read_buff_size = -1
    write_buff_size = 0

    read_timeout = 0

    def _init(self) -> None:
        self.connection = self.request
        self.rfile = self.connection.makefile('rb', self.read_buff_size)
        if self.read_timeout:
            self.connection.settimeout(self.read_timeout)
        self.wfile = self.connection.makefile('wb', self.write_buff_size)

    def _teardown(self) -> None:
        if not self.wfile.closed:
            try:
                self.wfile.flush()
            except:
                pass
        self.rfile.close()
        self.wfile.close()


class BaseServer:

    MAX_CLIENTS = 15
    __handler_threads = list()

    def __init__(self, host: str, port: int, request_handler_class) -> None:
        self.address = (host, port)
        self.request_handler_class: Type[AbstractRequestHandler] = request_handler_class
        self.selector = selectors.SelectSelector
        self._clients: List[socket.socket] = []

        self._logger = logging.getLogger('server')

        self._shutdown_event = threading.Event()
        self._shutdown_requested = False

        try:
            self.bind_server()
            self.activate_server()
        except:
            self.close_server()
            raise

    def serve(self, poll_interval: float = 0.5) -> None:
        self._shutdown_event.clear()
        self._logger.info('Starting polling for incoming connections')
        try:
            with self.selector() as selector:
                selector.register(self.socket, selectors.EVENT_READ)
                while not self._shutdown_requested:
                    ready = selector.select(poll_interval)
                    if self._shutdown_requested:
                        break
                    if ready:
                        self.handle_request()
        finally:
            self._shutdown_requested = False
            self._shutdown_event.set()

    def handle_request(self) -> None:
        try:
            client_socket, client_address = self.accept_client_connection()
        except OSError:
            return

        try:
            self.start_handler_thread(client_socket, client_address)
        except Exception as e:
            self.handle_error(client_socket, client_address)
            self.shutdown_client_request(client_socket)
        except:
            self.shutdown_client_request(client_socket)
            raise

    def start_handler_thread(self, client_sock, client_address) -> None:
        t = threading.Thread(
            target=self.process_client_request,
            args=(client_sock, client_address)
        )
        # Remove non-active threads
        self.__handler_threads = [thread for thread in self.__handler_threads if thread.is_alive()]

        self.__handler_threads.append(t)
        t.start()

    def process_client_request(self, request, client_address):
        self._logger.info('Spawning handler thread to process request from {}'.format(client_address))
        self._clients.append(request)
        try:
            self.invoke_request_handler(request, client_address)
        except Exception as e:
            self._logger.error(e)
            self.handle_error(request, client_address)
        finally:
            self.shutdown_client_request(request)

    def invoke_request_handler(self, client_socket, client_address) -> None:
        self.request_handler_class(client_socket, client_address, self).handle()

    def shutdown_client_request(self, client_sock) -> None:
        try:
            # explicitly shutdown client socket
            client_sock.shutdown(socket.SHUT_WR)
        except OSError:
            pass
        finally:
            client_sock.close()

    def accept_client_connection(self):
        return self.socket.accept()

    def bind_server(self):
        self.socket = socket.socket(self.addr_family, self.sock_type)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.address)

    def activate_server(self) -> None:
        self.socket.listen(self.MAX_CLIENTS)
        self._logger.info('Listening for incoming connections on {}'.format(self.socket.getsockname()))

    def close_server(self) -> None:
        self.socket.close()
        self._logger.info('Closing down handler threads')
        for thread in self.reset_handler_threads():
            thread.join()

    def reset_handler_threads(self):
        self.__handler_threads, result = [], self.__handler_threads
        return result

    def quit(self) -> None:
        self._logger.info('Shutdown request received, shutting down...')
        self._shutdown_requested = True
        self._shutdown_event.wait()

    def handle_error(self, client_socket, client_address) -> None:
        pass

    def broadcast_message(self, msg: str, sender_socket: socket.socket):
        for client in self._clients:
            # if client is not sender_socket:
            client.sendall(msg.encode())

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, logger: logging.Logger):
        self._logger = logger


class TcpServer(BaseServer):

    addr_family = socket.AF_INET

    sock_type = socket.SOCK_STREAM


class UdpServer(BaseServer):

    addr_family = socket.AF_INET

    sock_type = socket.SOCK_DGRAM

