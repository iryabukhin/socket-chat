import email.utils
import html
import io
import mimetypes
import os
import shutil
import socket
import sys
import time
import typing as t
import urllib.parse
from http import HTTPStatus

from ..sockserv import FileStreamRequestHandler


class HttpStreamRequestHandler(FileStreamRequestHandler):
    max_headers_count = 100
    max_header_len = 65536
    protocol_version = "HTTP/1.1"

    header_encoding = "iso-8859-1"

    server_name = "Python3/HttpTest"
    response_messages = {c: (c.phrase, c.description) for c in HTTPStatus.__members__.values()}

    def __init__(self, client_socket, client_address, server):
        super().__init__(client_socket, client_address, server)

        self.command: str = None
        self.uri: str = None
        self.request_line: str = None
        self.close: bool = True
        self._wbuff = list()

    def handle(self) -> None:
        # whether to close tcp connection after handling a request
        # if true, will allow handling multiple requests in one connection
        # as per RFC7230
        self.close = True

        self._handle_request()
        while not self.close:
            self._handle_request()

    def _handle_request(self):
        self.request_line = str(self.rfile.readline(2048), self.header_encoding).rstrip("\r\n")
        if not self.request_line:
            self.close = True
            return
        if not self.parse_request_line():
            return

        try:
            self.headers = self.parse_headers()
        except Exception as e:
            self.send_error_response(500, reason=str(e))
            return False

        connection_type = self.headers.get("Connection", "").lower()
        if connection_type == "close":
            self.close = True
        elif connection_type == "keep-alive":
            self.close = False

        try:
            self.dispatch_command_handler()
            self.wfile.flush()
        except socket.timeout as e:
            self.close = True
            return

    def parse_request_line(self) -> bool:
        if len(self.request_line.split()) != 3:
            return False

        command, uri, version = self.request_line.split()

        if not version.startswith("HTTP/"):
            self.send_error_response(
                HTTPStatus.BAD_REQUEST,
                'Malformed version string: "{}"'.format(version)
            )
            return False

        if version != self.protocol_version:
            self.send_error_response(
                HTTPStatus.HTTP_VERSION_NOT_SUPPORTED,
                'Unsupported protocol version: "{}"'.format(version)
            )
            return False

        self.command = command
        self.uri = uri

        return True

    def parse_headers(self) -> t.Dict[str, str]:
        # Primitive parser for HTTP request headers
        headers = list()
        while True:
            line = self.rfile.readline(self.max_header_len + 1)
            line = line.decode(self.header_encoding)

            # check if we have reached the end of header part of the request
            if line in ("", "\n", "\r\n"):
                break

            if len(line) > self.max_header_len:
                raise ValueError("Header length exceeds max amount")
            headers.append(line)
            if len(headers) > self.max_headers_count:
                raise ValueError("Header count exceeds max amount")

        rv = dict()
        for header_str in headers:
            field, value = header_str.rstrip("\r\n").split(":", 1)
            if value == "":
                raise ValueError("Missing header name")
            value = value.strip(' ').rstrip("\r\n")
            rv[field] = value

        return rv

    def send_response(self, code, message: str = None) -> None:
        if message is None:
            if code in self.response_messages:
                message = self.response_messages[code][0]
            else:
                message = ""

        self._wbuff.append(
            "{} {} {}\r\n".format(self.protocol_version, code, message).encode(
                self.header_encoding
            )
        )
        # Send timestamp according to RFC 2822
        self.send_datetime_header()
        self.send_server_header()

    def send_header(self, key, value):
        self._wbuff.append("{}: {}\r\n".format(key, value).encode(self.header_encoding))
        if key.lower() == "connection":
            if value.lower() == "close":
                self.close = True
            elif value.lower == "keep-alive":
                self.close = False

    def send_datetime_header(self) -> None:
        value = email.utils.formatdate(time.time(), usegmt=True)
        self.send_header("Date", value)

    def send_server_header(self) -> None:
        self.send_header("Server", self.server_name)

    def flush_header_buffer(self):
        self.wfile.write(b"".join(self._wbuff))
        self._wbuff = list()

    def end_headers(self):
        self._wbuff.append(b"\r\n")
        self.flush_header_buffer()

    def dispatch_command_handler(self) -> None:
        method_name = "handle_" + self.command
        if not hasattr(self, method_name):
            self.send_error_response(HTTPStatus.NOT_IMPLEMENTED)
            return

        handler = getattr(self, method_name)
        handler()

    def send_error_response(self, code: int, msg: str = None, reason: str = None) -> None:
        message = self.response_messages.get(code)
        if message is not None:
            short_msg, long_msg = message
        else:
            short_msg = long_msg = "?"

        if msg is None:
            msg = short_msg

        if reason is None:
            reason = long_msg

        self.send_response(code, msg)
        self.send_header("Connection", "close")

        body = '{} {}: Caused by "{}"'.format(code, msg, reason)
        self.send_header('Content-Type', 'text/plain')
        self.send_header('Content-Length', str(len(body)))
        self.end_headers()
        if self.command == 'HEAD':
            return
        self.wfile.write(body.encode(self.header_encoding))


class HttpServer(HttpStreamRequestHandler):
    def __init__(self, *args, cur_dir=None, **kwargs):
        if cur_dir is None:
            cur_dir = os.getcwd()
        self.cur_dir = cur_dir

        super().__init__(*args, **kwargs)

    def handle_HEAD(self):
        fp = self.send_head_data()
        if fp is not None:
            fp.close()

    def handle_GET(self) -> None:
        fp = self.send_head_data()
        # if file exists and it can be read, send it to the user
        if fp is not None:
            try:
                # copy file contents with no buffering
                shutil.copyfileobj(fp, self.wfile, -1)
            finally:
                fp.close()

    def handle_OPTIONS(self) -> None:
        commands = list()
        for prop in dir(self):
            if prop.startswith("handle_"):
                commands.append(prop.split("handle_")[1])

        self.send_response(HTTPStatus.OK)
        self.send_header("Allow", ", ".join(commands))
        self.send_header("Content-Length", 0)
        self.send_datetime_header()
        self.send_server_header()
        self.end_headers()
        return None

    def resolve_uri_path(self, uri: str) -> str:
        # ignore query and fragment part of uri
        uri = uri.split("?", 1)[0].split("#", 1)[0]
        uri = uri.strip()

        uri = urllib.parse.unquote(uri)
        words = uri.split("/")
        words = filter(None, words)  # Remove empty elements
        uri = self.cur_dir
        for word in words:
            if os.path.dirname(word) or word in (os.curdir, os.pardir):
                continue
            uri = os.path.join(uri, word)
        if uri.endswith("/"):
            uri += "/"

        return uri

    def resolve_mime_type(self, uri: str) -> str:
        base_path, extension = os.path.splitext(uri)
        extension = extension.lower()

        # default python stdlib mimetypes
        if not mimetypes.inited:
            mimetypes.init()
        mimetypes_ext_map = mimetypes.types_map.copy()
        if extension in mimetypes_ext_map:
            return mimetypes_ext_map[extension]
        return mimetypes_ext_map['']

    def send_head_data(self) -> t.Optional[t.IO]:
        path = self.resolve_uri_path(self.uri)
        if os.path.isdir(path):
            parts = urllib.parse.urlsplit(self.uri)
            if not parts.path.endswith("/"):
                # redirect browser to directory listing page
                self.send_response(HTTPStatus.MOVED_PERMANENTLY)
                new_parts = (parts[0], parts[1], parts[2] + "/", parts[3], parts[4])
                new_url = urllib.parse.urlunsplit(new_parts)
                self.send_header("Location", new_url)
                self.end_headers()
                return None
            else:
                return self.list_directory(path)

        mime_type = self.resolve_mime_type(path)
        if path.endswith("/"):
            self.send_error_response(HTTPStatus.NOT_FOUND, "Not found")
            return None
        try:
            fp = open(path, "rb")
        except OSError:
            self.send_error_response(HTTPStatus.NOT_FOUND, "File not found")
            return None

        try:
            # Send info about the file in specific headers
            fs = os.fstat(fp.fileno())
            filesize = fs[6]
            last_modified = email.utils.formatdate(fs.st_mtime, usegmt=True)

            self.send_response(HTTPStatus.OK)
            self.send_header("Content-Type", mime_type)
            self.send_header("Content-Length", str(filesize))
            self.send_header("Last-Modified", last_modified)
            self.end_headers()
            # return file obj in case its HEAD request
            return fp
        except:
            fp.close()
            raise

    def list_directory(self, path: str) -> t.Optional[t.IO]:
        try:
            listing = os.listdir(path)
        except OSError:
            self.send_error_response(404, "Unable to get directory listing")
            return

        rv = []

        listing.sort(key=lambda f: f.lower())

        escaped_path = urllib.parse.unquote(self.uri)
        sys_encoding = sys.getfilesystemencoding()

        page_title = "Directory listing for {}".format(escaped_path)

        rv.append(
            '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '
            '"http://www.w3.org/TR/html4/strict.dtd">'
        )
        rv.append("<html>\n<head>")
        rv.append(
            '<meta http-equiv="Content-Type" content="text/html; charset={}">'.format(
                sys_encoding
            )
        )
        rv.append("<title>{}</title>\n</head>".format(page_title))
        rv.append("<body>\n<h1>{}</h1>".format(page_title))
        rv.append("<br>\n<ul>")

        for filename in listing:
            abspath = os.path.join(path, filename)
            name = href = filename
            if os.path.isdir(abspath):
                href = name = name + "/"

            rv.append(
                '<li><a href="{}">{}</a></li>'.format(
                    urllib.parse.quote(href), name
                )
            )

        rv.append("</ul>\n</body></html>\n")
        content = "\n".join(rv).encode(sys_encoding)

        self.send_response(HTTPStatus.OK)
        self.send_header("Content-Type", "text/html; charset={}".format(sys_encoding))
        self.send_header("Content-Length", str(len(content)))
        self.end_headers()

        fp = io.BytesIO()
        fp.write(content)
        fp.seek(0)

        return fp
