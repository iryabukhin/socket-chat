from typing import Union
import config
from peewee import DoesNotExist
from models import Message as MessageModel, User as UserModel

from src.sockserv.server import AbstractRequestHandler
from uitls import hash_password, check_password

conf = config.Config("settings.cfg")


class ChatRequestHandler(AbstractRequestHandler):
    BUFFER_SIZE = 1024

    def handle(self) -> None:
        if not self.perform_authentication():
            self.send("Authentication failed!")
            self.request.close()
            return

        self.send(f"Welcome, {self.user.login}!")
        self.send("Last 15 messages:")
        last_messages = (
            MessageModel.select().order_by(MessageModel.sent_at).limit(15).distinct()
        )
        for msg in last_messages:
            msg_strings = list()
            msg_strings.append(
                f"[{msg.sent_at.strftime(conf.message_time_format)}] {msg.user.login}: {msg.text}"
            )
            data = "\n".join(msg_strings)
            self.send(data)

        self.send("Enter your message: ")
        self.wait_for_incoming_messages()

    def perform_authentication(self) -> bool:
        self.send("Username: ", newline=False)
        login = self.recv()

        self.send("Password: ", newline=False)
        password = self.recv()

        try:
            u = UserModel.get(UserModel.login == login)
            if not check_password(password, u.password):
                self.send("Invalid password!")
                return False
        except DoesNotExist:
            u = UserModel(login=login, password=hash_password(password.encode()))
            u.save()
            self.send("Registration successful")

        self.user = u
        return True

    def send(self, msg: Union[str, bytes], newline=True):
        if isinstance(msg, str):
            msg = str.encode(msg)
        if newline:
            msg += b"\n"

        self.request.sendall(msg)

    def recv(self, buffer_size=None) -> str:
        if not buffer_size:
            buffer_size = conf['buffer_size']
        data = self.request.recv(buffer_size)
        data = data.decode('utf-8').strip("\n")
        return data

    def wait_for_incoming_messages(self) -> None:
        while True:
            data = self.recv()
            if not data:
                break
            msg = MessageModel(text=data, user=self.user)
            msg.save()
            self.server.broadcast_message(
                self.format_message(msg),
                self.request
            )

    def format_message(self, msg: MessageModel) -> str:
        return "[{}] {}: {}\n".format(
            msg.sent_at.strftime(conf['message_time_format']),
            msg.user.login,
            msg.text
        )