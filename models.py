from peewee import *
import datetime

db = SqliteDatabase('chat.db')


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    login = CharField(unique=True, max_length=127)
    password = CharField()
    last_seen_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        table_name = 'users'


class Message(BaseModel):
    user = ForeignKeyField(User, backref='messages')
    text = TextField()
    sent_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        table_name = 'message'
