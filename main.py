from conf import conf
from src.sockserv.server import TcpServer
from src.http import HttpServer

if __name__ == '__main__':
    host = conf['host']
    port = int(conf['port'])

    server = TcpServer(host, port, HttpServer)

    try:
        server.serve()
    except KeyboardInterrupt:
        server.close_server()
