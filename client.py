import getpass
import threading
import os, sys
import socket
import pickle
from conf import conf
from typing import List, Dict, Union, Optional, Tuple


class ClientThread:
    def __init__(self, host: str, port: int):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.sock.connect((self.host, self.port))

    def send_message(self, msg: str):
        self.sock.send(msg.encode())
        resp = self.sock.recv(conf['buffer_size'])
        return resp

    def try_login(self, login: str, password: str) -> bool:
        self.sock.send(login.encode())
        self.sock.send(password.encode())

        resp = self.sock.recv(conf['buffer_size'])
        return resp



if __name__ == '__main__':
    client = ClientThread(conf['host'], int(conf['port']))
    try:
        login = input('Username: ')
        password = getpass.getpass(prompt='Password: ')

        server_response = client.try_login(login, password)
    except KeyboardInterrupt:
        print('Exiting...')