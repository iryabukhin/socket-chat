from dataclasses import dataclass, field
from datetime import datetime


@dataclass
class Message:
    text: str
    created_at: datetime = field(default_factory=datetime.now)
